# Project Management App

# Setup Guide

- Clone the project repository from BitBucket.
  ```bash
  git clone <project_url>
  ```
- Open the cloned project using an IDE. VS Code is preferred.
- Open the `pubspec.yaml` file and press `Ctrl+S`. This will install the dependencies.
- After you install the packages, create a `.env` file and copy the configuration below:
  ```bash
  API_SERVER_URL=http://127.0.0.1:400/api
  STATIC_SERVER_URL=http://10.0.2.2:4000/api
  ```
- To run the project, open the `main.dart` file then click Debug or press `Ctrl+F5`. (Note: Make sure that the API server is running to make sure that the app runs properly.).
- To run the server, open a terminal and go to the `csp_api` folder then run this command:
  ```bash
  dart run bin/main.dart
  ```

### Test User Credentials

| Email                   | Password      |
| ----------------------- | ------------- |
| contractor@gmail.com    | contractor    |
| subcontractor@gmail.com | subcontractor |
| assembly-team@gmail.com | assembly-team |

# Packages

[http ^0.13.3](https://pub.dev/packages/http)

- This package contains a set of high-level functions and classes that make it easy to consume HTTP resources. It's multi-platform, and supports mobile, desktop, and the browser.

[provider ^5.0.0](https://pub.dev/packages/provider)

- A wrapper around InheritedWidget to make them easier to use and more reusable.

[shared_preferences ^2.0.6](https://pub.dev/packages/shared_preferences)

- Wraps platform-specific persistent storage for simple data (NSUserDefaults on iOS and macOS, SharedPreferences on Android, etc.). Data may be persisted to disk asynchronously, and there is no guarantee that writes will be persisted to disk after returning, so this plugin must not be used for storing critical data.

[image_picker ^0.8.3+2](https://pub.dev/packages/image_picker)

- Flutter plugin for selecting images from the Android and iOS image library, and taking new pictures with the camera.

[flutter_dotenv ^5.0.0](https://pub.dev/packages/flutter_dotenv)

- Load configuration at runtime from a .env file which can be used throughout the application.

[url_launcher ^6.0.9](https://pub.dev/packages/url_launcher)

- A Flutter plugin for launching a URL. Supports iOS, Android, web, Windows, macOS, and Linux.

---

# Versions

- [Flutter 2.2.3](https://flutter.dev/docs/get-started/install)

- [sdk >=2.12.0 <3.0.0](https://flutter.dev/docs/development/tools/sdk/releases)

---

# Features

**Login Feature**

- Save token to app state
- Save token to local storage

**Contractor**

- Show all projects
- Add project
- Assign project to subcontractor
- Review finished project tasks

**Subcontractor**

- Show assigned projects
- Show project tasks
- Add task & assign to assembly team

**Assembly Team**

- Show projects with assigned tasks
- Show project tasks
- Tag task as completed
