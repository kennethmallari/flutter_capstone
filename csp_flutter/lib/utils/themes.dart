import 'package:flutter/material.dart';

var btnDefaultTheme = ElevatedButton.styleFrom(
    primary: Colors.white, // background color
    side: BorderSide(width: 1.0, color: Colors.black),
    onPrimary: Colors.black, // text color
    padding: EdgeInsets.all(20.0),
);

