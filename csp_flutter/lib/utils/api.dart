import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;
import 'package:flutter_dotenv/flutter_dotenv.dart';

import '/models/task.dart';
import '/models/user.dart';
import '/models/project.dart';


class API {
    // AVD routes requests to 127.0.0.1 (localhost) through 10.0.2.2 in the virtual device.
    final String _url = dotenv.env['API_SERVER_URL'] as String;
    final String? _accessToken;

    API([ this._accessToken ]);

    Future<User> login({ 
        required String email, 
        required String password 
    }) async {
        try {
            final response = await http.post(
                Uri.parse('$_url/users/login'),
                headers: <String, String>{
                    'Content-Type': 'application/json; charset=UTF-8',
                },
                body: jsonEncode({
                    'email': email,
                    'password': password
                })
            );

            if (response.statusCode == 200) {
                return User.fromJson(jsonDecode(response.body));
            } else {
                throw Exception('User authentication procedure failed.');
            }
        } catch (exception) {
            throw exception;
        }
    }

    Future<bool> addProject({ 
        required String name, 
        required String description 
    }) async {
        try {
            final response = await http.post(
                Uri.parse('$_url/projects'),
                headers: <String, String>{
                    'Content-Type': 'application/json; charset=UTF-8',
                    'Authorization': 'Bearer $_accessToken'
                },
                body: jsonEncode({
                    'name': name,
                    'description': description
                })
            );

            if (response.statusCode == 200) {
                return jsonDecode(response.body);
            } else {
                throw Exception('Project creation procedure failed.');
            }
        } catch (exception) {
            throw exception;
        }
    }

    Future<List<Project>> getProjects() async {
        try {
            final response = await http.get(
                Uri.parse('$_url/projects'),
                headers: <String, String>{
                    'Content-Type': 'application/json; charset=UTF-8',
                    'Authorization': 'Bearer $_accessToken'
                }
            );

            if (response.statusCode == 200) {
                return (jsonDecode(response.body) as List).map((project) => Project.fromJson(project)).toList();
            } else {
                throw Exception('Tasks cannot be retrieved from the server.');
            }
        } catch (exception) {
            throw exception;
        }
    }

    Future<bool> assignProject({ 
        required int? assignedTo, 
        required int? projectId 
    }) async {
        try {
            final response = await http.put(
                Uri.parse('$_url/projects/assign'),
                headers: <String, String>{
                    'Content-Type': 'application/json; charset=UTF-8',
                    'Authorization': 'Bearer $_accessToken'
                },
                body: jsonEncode({
                    'assignedTo': assignedTo,
                    'projectId': projectId
                })
            );

            if (response.statusCode == 200) {
                return jsonDecode(response.body);
            } else {
                throw Exception('Projects assignment procedure failed.');
            }
        } catch (exception) {
            throw exception;
        }
    }

    Future<bool> addTask({ 
        required String title, 
        required String description, 
        required int assignedTo, 
        required int projectId 
    }) async {
        try {
            final response = await http.post(
                Uri.parse('$_url/tasks'),
                headers: <String, String>{
                    'Content-Type': 'application/json; charset=UTF-8',
                    'Authorization': 'Bearer $_accessToken'
                },
                body: jsonEncode({
                    'title': title,
                    'description': description,
                    'assignedTo': assignedTo,
                    'projectId': projectId
                })
            );

            if (response.statusCode == 200) {
                return jsonDecode(response.body);
            } else {
                throw Exception('Task creation procedure failed.');
            }
        } catch (exception) {
            throw exception;
        }
    }

    Future<List<Task>> getTasks(int? projectId) async {
        try {
            final response = await http.get(
                Uri.parse('$_url/tasks?projectId=$projectId'),
                headers: <String, String>{
                    'Content-Type': 'application/json; charset=UTF-8',
                    'Authorization': 'Bearer $_accessToken'
                }
            );

            if (response.statusCode == 200) {
                return (jsonDecode(response.body) as List).map((task) => Task.fromJson(task)).toList();
            } else {
                throw Exception('Tasks cannot be retrieved from the server.');
            }
        } catch (exception) {
            throw exception;
        }
    }

    Future<bool> assignTask({ 
        required int? assignedTo, 
        required int? taskId 
    }) async {
        try {
            final response = await http.put(
                Uri.parse('$_url/tasks/assign'),
                headers: <String, String>{
                    'Content-Type': 'application/json; charset=UTF-8',
                    'Authorization': 'Bearer $_accessToken'
                },
                body: jsonEncode({
                    'assignedTo': assignedTo,
                    'taskId': taskId
                })
            );

            if (response.statusCode == 200) {
                return jsonDecode(response.body);
            } else {
                throw Exception('Task assignment procedure failed.');
            }
        } catch (exception) {
            throw exception;
        }
    }

    Future<bool> updateTaskStatus({ 
        required int? taskId, 
        required String? status 
    }) async {
        try {
            final response = await http.put(
                Uri.parse('$_url/tasks/update-status'),
                headers: <String, String>{
                    'Content-Type': 'application/json; charset=UTF-8',
                    'Authorization': 'Bearer $_accessToken'
                },
                body: jsonEncode({
                    'taskId': taskId,
                    'status': status
                })
            );

            if (response.statusCode == 200) {
                return jsonDecode(response.body);
            } else {
                throw Exception('Task assignment procedure failed.');
            }
        } catch (exception) {
            throw exception;
        }
    }

    Future<Task> addTaskImage({ 
        required int id, 
        required String filePath 
    }) async {
        try {
            var request = http.MultipartRequest('POST', Uri.parse('$_url/tasks/image'));

            request.fields['id'] = id.toString();
            request.files.add(
                http.MultipartFile(
                    'picture',
                    File(filePath).readAsBytes().asStream(),
                    File(filePath).lengthSync(),
                    filename: filePath.split('/').last
                )
            );

            var response = await request.send();
            var responseData = await response.stream.toBytes();
            var responseString = String.fromCharCodes(responseData);

            if (response.statusCode == 200) {
                return Task.fromJson(jsonDecode(responseString));
            } else {
                throw Exception('Task image upload procedure failed.');
            }
        } catch (exception) {
            throw exception;
        }
    }

    Future<List<User>> getUsersByDesignation(String designation) async {
        try {
            final response = await http.get(
                Uri.parse('$_url/users/$designation'),
                headers: <String, String>{
                    'Content-Type': 'application/json; charset=UTF-8',
                    'Authorization': 'Bearer $_accessToken'
                }
            );

            if (response.statusCode == 200) {
                return (jsonDecode(response.body) as List).map((user) => User.fromJson(user)).toList();
            } else {
                throw Exception('Users cannot be retrieved from the server.');
            }
        } catch (exception) {
            throw exception;
        }
    }
}