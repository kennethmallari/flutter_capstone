import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

_launchURL() async {
    const url = 'https://www.linkedin.com/in/kenneth-christian-mallari-620994126/';
    if (await canLaunch(url)) {
        await launch(url);
    } else {
        throw 'Could not launch $url';
    }
}

var contactCard = Container(
    width: double.infinity,
    padding: EdgeInsets.all(20.0),
    child: Column(
        children: <Widget>[
            Card(
                color: Color.fromRGBO(20, 45, 68, 1),
                child: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Column(
                        children:<Widget>[
                            Center(
                                child: Text(
                                    'Get in touch.',
                                    style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold,
                                    letterSpacing: 1
                                    ),
                                )
                            ),
                            Container(
                                margin: EdgeInsets.only(top:16.0,bottom: 16.0),
                                width: double.infinity,
                                child: Row(
                                    children: [
                                        Expanded(
                                            child:new ElevatedButton(
                                                onPressed: _launchURL, 
                                                child: new Text(
                                                    'Linkedin',
                                                    style: TextStyle(
                                                        color: Colors.black,
                                                        fontWeight: FontWeight.bold
                                                    ),
                                                ),
                                                style: ElevatedButton.styleFrom(
                                                    primary: Colors.white
                                                ),
                                            ),
                                        ) 
                                    ]  
                                )                                                    
                            ),
                            Container(
                                width: double.infinity,
                                margin: EdgeInsets.only(bottom: 16.0),
                                child: const Divider(
                                    color: Colors.white,
                                    height: 25,
                                    thickness: 2,
                                    indent: 5,
                                    endIndent: 5,
                                ),
                            ),
                            Container(
                                margin: EdgeInsets.only(bottom: 32.0),
                                width: double.infinity,
                                child: Text(
                                    'Email: kennethmallari04@gmail.com\n\nPhone: 0905-000-0000\n\nAddress: Lubao,Pampanga',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.normal,
                                        letterSpacing: 1
                                    ),
                                    textAlign: TextAlign.start, 
                                )
                            ),
                        ],  
                    )
                ),
            ),
        ],
    )
);