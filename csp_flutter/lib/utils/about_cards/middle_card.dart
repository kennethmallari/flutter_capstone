import 'package:flutter/material.dart';

var middleCard = Container(
    width: double.infinity,
    padding: EdgeInsets.all(20.0),
    child: Column(
        children: <Widget>[
            Card(
                color: Color.fromRGBO(20, 45, 68, 1),
                child: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Column(
                        children:<Widget>[
                            Center(
                                child: Text(
                                    '| Introduction |',
                                    style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold,
                                    letterSpacing: 1
                                    ),
                                )
                            ),
                            Container(
                                margin: EdgeInsets.only(top:16.0,bottom: 16.0),
                                width: double.infinity,
                                child: Text(
                                    '"Hi, I\'m Kenneth! I  aspire to become a professional full-stack developer. I am looking forward to create apps and systems that will impact our soiety in a positive way. During my free time, I love watching Netflix, documentaries, and Youtube travel vlogs. I also plan to travel the world in the near future."',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.normal,
                                        fontStyle: FontStyle.italic,
                                        letterSpacing: 1
                                    ),
                                    textAlign: TextAlign.start, 
                                )
                            ),
                            Container(
                                width: double.infinity,
                                margin: EdgeInsets.only(bottom: 16.0),
                                child: const Divider(
                                    color: Colors.white,
                                    height: 25,
                                    thickness: 2,
                                    indent: 5,
                                    endIndent: 5,
                                ),
                            ),
                            Center(
                                child: Text(
                                    'Work Experience',
                                    style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold,
                                    letterSpacing: 1
                                    ),
                                )
                            ),
                            Container(
                                margin: EdgeInsets.only(top:16.0,bottom: 32.0),
                                width: double.infinity,
                                child: Text(
                                    'Small Business Consultant in a BPO Company - (2016 -2021)',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.normal,
                                        letterSpacing: 1
                                    ),
                                    textAlign: TextAlign.center, 
                                )
                            ),
                            Center(
                                child: Text(
                                    'Skills',
                                    style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20.0,
                                    fontWeight: FontWeight.bold,
                                    letterSpacing: 1
                                    ),
                                )
                            ),
                            Container(
                                margin: EdgeInsets.only(top:24.0,bottom: 20.0),
                                width: double.infinity,
                                child: Text(
                                    'Web Development . Flutter . Dart . Software Development . Consultative Sales',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 16.0,
                                        fontWeight: FontWeight.normal,
                                        letterSpacing: 1
                                    ),
                                    textAlign: TextAlign.center, 
                                )
                            ),
                        ],  
                    )
                ),
            ) 
        ],
    )
);