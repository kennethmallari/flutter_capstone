import 'package:flutter/material.dart';


var headingCard = Container(
    width: double.infinity,
    child: Column(
        children: <Widget>[
            Card(
                color: Color.fromRGBO(20, 45, 68, 1),
                shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                child: Padding(
                    padding: EdgeInsets.all(16.0),
                    child: Column(
                        children:<Widget>[
                            Center(
                                child: CircleAvatar(
                                    backgroundImage:AssetImage('../assets/me1.jpg') ,
                                    radius: 70
                                )
                            ),
                            Container(
                                width: double.infinity,
                                margin: EdgeInsets.only(top:16.0,bottom: 16.0),
                                child: Text(
                                    'Kenneth Christian Mallari',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 32.0,
                                        fontWeight: FontWeight.bold
                                    ),
                                    textAlign: TextAlign.center, 
                                )
                            )
                        ],  
                    )
                ),
            ),  
        ],
    )
);