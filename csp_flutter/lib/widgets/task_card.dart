import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

//My personal imports.
import '/models/task.dart';
import '/providers/user_provider.dart';
import '/screens/task_detail_screen.dart';
import '/utils/api.dart';
import '/utils/functions.dart';
import '/utils/themes.dart';

class TaskCard extends StatefulWidget {
    final Task _task;
    final Function _reloadTasks;

    TaskCard(this._task, this._reloadTasks);

    @override
    _TaskCard createState() => _TaskCard();
}
class _TaskCard extends State<TaskCard> {    
    @override
    Widget build(BuildContext context) {
        final String? designation = Provider.of<UserProvider>(context).designation;
        final String? accessToken = context.read<UserProvider>().accessToken;

        Widget rowTaskInfo = Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
                Expanded(
                    child: Column(
                        // Modify both the main and cross axis alignment.
                        crossAxisAlignment: CrossAxisAlignment.start,
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                            Text(
                                widget._task.title, 
                                style: TextStyle(
                                    fontSize:  20.0
                                ),
                            ),
                            Text(widget._task.description)
                        ]
                    )
                ),
                Chip(label: Text(widget._task.status!))
            ]
        );

        Widget btnStart = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: ElevatedButton(
                child: Text('Start'),
                onPressed: () {
                    API(accessToken).updateTaskStatus(taskId:widget._task.id, status: 'Ongoing')
                    .then((value)=> widget._reloadTasks()).catchError((error){
                        showSnackBar(context, error.message);
                    });
                },
            )
        );

        Widget btnFinish = Container(
            margin: EdgeInsets.symmetric(horizontal: 4),
            child: ElevatedButton(
                child: Text('Finish'),
                onPressed: () {
                    API(accessToken).updateTaskStatus(taskId:widget._task.id, status: 'Completed')
                    .then((value)=> widget._reloadTasks()).catchError((error){
                        showSnackBar(context, error.message);
                    });
                 },
            )
        );

        Widget btnAccept = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: ElevatedButton(
                child: Text('Accept'),
                onPressed: () {
                    API(accessToken).updateTaskStatus(taskId:widget._task.id, status: 'Accepted')
                    .then((value)=> widget._reloadTasks()).catchError((error){
                        showSnackBar(context, error.message);
                    });
                 },
            )
        );

        Widget btnReject = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: ElevatedButton(
                child: Text('Reject'),
                style: btnDefaultTheme,
                onPressed: () { 
                    API(accessToken).updateTaskStatus(taskId:widget._task.id, status: 'Rejected')
                    .then((value)=> widget._reloadTasks()).catchError((error){
                        showSnackBar(context, error.message);
                    });
                },
            )
        );

        Widget btnDetail = Container(
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            child: GestureDetector(
                child: Text(
                    'Detail',
                    style: TextStyle(decoration: TextDecoration.underline, fontWeight: FontWeight.bold),
                ),
                onTap: () async {
                    await Navigator.push(context, MaterialPageRoute(builder: (context) => TaskDetailScreen(widget._task)));
                    widget._reloadTasks();
                },
            )
        );

        Widget rowActions = Row(
            children: [
                btnDetail,
                Spacer(),   
                // Show btnStart if designation is assembly team and task status is pending, else show an empty container.
                (designation == 'assembly-team' && widget._task.status == 'Pending') ? btnStart : Container(),
                // Show btnFinish if designation is assembly team and task status is ongoing, else show an empty container.
                (designation == 'assembly-team' && widget._task.status == 'Ongoing') ? btnFinish : Container(),
                // Show btnAccept if designation is contractor and task status is completed or rejected, else show an empty container.
                (designation == 'contractor' && (widget._task.status == 'Completed' || widget._task.status == 'Rejected')) ? btnAccept : Container(),
                // Show btnReject if designation is contractor and task status is completed or accepted, else show an empty container.
                (designation == 'contractor' && (widget._task.status == 'Completed' || widget._task.status =='Accepted')) ? btnReject : Container(),
            ]
        );

        return Card(
            child: Padding(
                padding: EdgeInsets.all(16.0), 
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                        rowTaskInfo,
                        SizedBox(height: 16.0),
                        rowActions
                    ]
                )
            )
        );
    }
}