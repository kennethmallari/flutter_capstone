import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

//My personal imports.
import '/models/project.dart';
import '/providers/user_provider.dart';
import '/screens/task_list_screen.dart';
import '/utils/themes.dart';
import '/widgets/assign_project_dialog.dart';

class ProjectCard extends StatefulWidget {
    final Project _project;
    final Function _reloadProjects;

    ProjectCard(this._project, this._reloadProjects);

    @override
    _ProjectCard createState() => _ProjectCard();
}

class _ProjectCard extends State<ProjectCard> {    
    @override
    Widget build(BuildContext context) {
        final String? designation = Provider.of<UserProvider>(context).designation;
        print(designation);

        Widget ltProjectInfo = ListTile(
            title: Text(
                widget._project.name!,
                style: TextStyle(
                    fontWeight: FontWeight.bold
                ),
            ),
            subtitle: Wrap(
                direction: Axis.vertical, 
                children: [
                    Text(widget._project.description!)
                ]
            )
        );

        Widget btnAssign = Container(
            margin: EdgeInsets.symmetric(horizontal: 4),
            child: ElevatedButton(
                child: Text(
                    'Assign',
                    style: TextStyle(fontWeight: FontWeight.bold),
                ),
                style: btnDefaultTheme,                
                onPressed: () {
                    showDialog(
                        context: context,
                        builder: (BuildContext context) => AssignProjectDialog(widget._project.id)
                    ).then((value) {
                        widget._reloadProjects();
                    });
                },
            )
        );

        Container btnTasks = Container(
            margin: EdgeInsets.symmetric(horizontal: 4),
            child: ElevatedButton(
                child: Text('Tasks'),
                onPressed: () async {
                    await Navigator.push(context, MaterialPageRoute(builder: (context) => TaskListScreen(widget._project.id)));
                    widget._reloadProjects();
                },
            )
        );

        Row rowActions = Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
                // Show the btnAssign button if designation is contractor and project.assignedTo is null, else show an empty container.
                 (widget._project.assignedTo == null && designation == 'contractor' ) ? btnAssign : Container(),
                 btnTasks
            ]

            
        );
        
        return Card(
            child: Padding(
                padding: EdgeInsets.all(16.0), 
                child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: [
                        ltProjectInfo,
                        rowActions
                    ]
                )
            )
        );
    }
}