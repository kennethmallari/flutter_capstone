import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '/providers/user_provider.dart';
import '/utils/api.dart';
import '/utils/functions.dart';
import '/utils/themes.dart';

class AssignProjectDialog extends StatefulWidget {
    final int? _projectId;

    AssignProjectDialog([ this._projectId ]);

    @override
    _AssignProjectDialog createState() => _AssignProjectDialog();
}

class _AssignProjectDialog extends State<AssignProjectDialog> {
    final _formKey = GlobalKey<FormState>();

    List<DropdownMenuItem> _subcontractorOptions = [];
    int? _assignedTo;

    void assignProject(BuildContext context) {
        final String? accessToken = context.read<UserProvider>().accessToken;
        API(accessToken).assignProject(assignedTo: _assignedTo, projectId:widget._projectId);

    }

    @override
    void initState() {
        super.initState();

        WidgetsBinding.instance!.addPostFrameCallback((timeStamp) {
            final String? accessToken = context.read<UserProvider>().accessToken;

            API(accessToken).getUsersByDesignation('subcontractors').then((subcontractors) {
                setState(() {
                    _subcontractorOptions = subcontractors.map((subcontractor) {
                        return DropdownMenuItem(
                            child: Text(subcontractor.email!),
                            value: subcontractor.id
                        );
                    }).toList();
                });
            }).catchError((error) {
                showSnackBar(context, error.message);
            });
        });
    }

    @override
    Widget build(BuildContext context) {
        final String? accessToken = Provider.of<UserProvider>(context).accessToken;

        DropdownButtonFormField txtSubcontractor = DropdownButtonFormField(
            decoration: InputDecoration(labelText: 'Subcontractor'),
            items: _subcontractorOptions,
            onChanged: (value) {
                setState(() {
                  _assignedTo = value;
                });
            }
        );

        Widget formAssignProject = Form(
            key: _formKey,
            child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                    txtSubcontractor
                ]
            )
        );

        return AlertDialog(
            title: Text(
                'Assign Project',
                style: TextStyle(fontWeight: FontWeight.bold),
            ),
            content: Container(
                child: formAssignProject
            ),
            actions: [
                ElevatedButton(
                    child: Text('Assign'),
                    style: ElevatedButton.styleFrom(
                        onPrimary: Colors.white,
                    ),
                    onPressed: () {
                        if (_formKey.currentState!.validate()) {
                            assignProject(context);
                            Navigator.of(context).pop();
                        } else {
                            showSnackBar(context, 'Select a resolver to assign the task.');
                        }
                    },
                ),
                ElevatedButton(
                    child: Text(
                        'Cancel',
                        style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    style: btnDefaultTheme,
                    onPressed: () {
                        Navigator.of(context).pop();
                    }
                ),
            ],
        );
    }
}