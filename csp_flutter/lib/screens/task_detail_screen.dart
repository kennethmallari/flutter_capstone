import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';

//My personal imports.
import '/models/task.dart';
import '/providers/user_provider.dart';

class TaskDetailScreen extends StatefulWidget {
    final Task? _task;

    TaskDetailScreen([this._task]);

    @override
    _TaskDetailScreen createState() => _TaskDetailScreen();
}

class _TaskDetailScreen extends State<TaskDetailScreen> {  
    Future<Task>? _futureUploadImage;

    FutureBuilder<Task> _futureImageResponse() {
        return FutureBuilder<Task>(
            future: _futureUploadImage,
            builder: (context, snapshot) {
                if (snapshot.hasData) {
                    return Image.network('${dotenv.env['STATIC_SERVER_URL'] as String}/' + snapshot.data!.imageLocation!);
                }

                return Center(
                    child: CircularProgressIndicator()
                );
            },
        );
    }

    Image _showImage(Task task) {
        if (task.imageLocation == null) {
            return Image.network('https://via.placeholder.com/150');
        } else {
            return Image.network('${dotenv.env['STATIC_SERVER_URL'] as String}/' + task.imageLocation!);
        }
    }

    @override
    Widget build(BuildContext context) {
        final String? accessToken = Provider.of<UserProvider>(context).accessToken;
        final String? designation = Provider.of<UserProvider>(context).designation;

        final bool isTaskPending = widget._task!.status == 'Pending';
        final bool isTaskOngoing = widget._task!.status == 'Ongoing';
        final bool isTaskCompleted = widget._task!.status == 'Completed';

        Widget btnUpload = Container(
            margin: EdgeInsets.only(top: 10.0),
            child: ElevatedButton(
                onPressed: () async { 
                    var selectedImage = await ImagePicker().getImage(source: ImageSource.gallery);
                }, 
                child: Text('Upload an Image')
            )
        );

        Widget taskInfo = Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
                Text('Title', style: TextStyle(color: Colors.grey)),
                TextFormField(
                    initialValue: widget._task!.title,
                    readOnly: true,
                ),
                SizedBox(height: 8.0),
                Text('Description', style: TextStyle(color: Colors.grey)),
                TextFormField(
                    initialValue: widget._task!.description,
                    readOnly: true,
                ),
                SizedBox(height: 8.0),
                Text('Status', style: TextStyle(color: Colors.grey)),
                Chip(label: Text(widget._task!.status!))
            ]
        );

        return Scaffold(
            appBar: AppBar(title: Text('Task Detail')),
            body: Container(
                width: double.infinity,
                margin: EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                child: SingleChildScrollView(
                    child: Column(
                        children: [
                            Container(
                                margin: EdgeInsets.only(top: 16.0),
                                child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                        Text('Task Image'),
                                        SizedBox(height: 8.0),
                                        (_futureUploadImage != null) ? _futureImageResponse() : _showImage(widget._task!)
                                    ]
                                )
                            ),
                            (designation == 'assembly-team' && (isTaskPending || isTaskOngoing || isTaskCompleted)) ? btnUpload : Container(),
                            SizedBox(height: 16.0),
                            taskInfo
                        ]
                    )
                )
            )
        );
    }
}