import 'dart:async';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

//My personal imports.
import '/models/user.dart';
import '/providers/user_provider.dart';
import '/utils/api.dart';
import '/utils/functions.dart';

class LoginScreen extends StatefulWidget {
    @override
    _LoginScreen createState() => _LoginScreen();
}

class _LoginScreen extends State<LoginScreen> {
    Future<User>? _futureLogin;

    final _formKey = GlobalKey<FormState>();
    final _tffEmailController = TextEditingController();
    final _tffPasswordController = TextEditingController();

    void _login(BuildContext context) {
        setState(() {
          _futureLogin = API().login(
              email: _tffEmailController.text,
              password: _tffPasswordController.text
            ).catchError((error){
                showSnackBar(context, error.message);
            });
        });

    }

    void _goToHomepage(data) async {
        User user = data as User;

        final prefs = await SharedPreferences.getInstance();
        final Function setAccessToken = Provider.of<UserProvider>(context, listen: false).setAccessToken;
        final Function setDesignation = Provider.of<UserProvider>(context, listen: false).setDesignation;

        if (user.accessToken != null) {
            setAccessToken(user.accessToken);
            setDesignation(user.designation);

            prefs.setString('accessToken', user.accessToken!);
            prefs.setString('designation', user.designation!);

            Navigator.pushReplacementNamed(context, '/project-list');
        } else {
            showSnackBar(context, 'User not found.');
            Navigator.pushReplacementNamed(context,'/');
        }
    }

    @override
    Widget build(BuildContext context) {
        Widget txtEmail = TextFormField(
            decoration: InputDecoration(
                labelText: 'Email',
                labelStyle: TextStyle(
	            color: Colors.black
                ),
                enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                ),
                focusedBorder: UnderlineInputBorder(
                    borderSide:BorderSide(color: Colors.black),
                )
            ),
            keyboardType: TextInputType.emailAddress,
            controller: _tffEmailController,
            textInputAction: TextInputAction.next,
            cursorColor: Colors.black,
            style: TextStyle(color: Colors.black),
            validator: (value) {
                return (value != null && value.isNotEmpty && value.contains('@')) ? null : 'Invalid email.';
            }
        );

        Widget txtPassword = TextFormField(
            decoration: InputDecoration(
                labelText: 'Password',
                labelStyle: TextStyle(
	            color: Colors.black
                ),
                enabledBorder: UnderlineInputBorder(
                    borderSide: BorderSide(color: Colors.black),
                ),
                focusedBorder: UnderlineInputBorder(
                    borderSide:BorderSide(color: Colors.black),
                )
            ),
            obscureText: true,
            controller: _tffPasswordController,
            textInputAction: TextInputAction.done,
            cursorColor: Colors.black,
            style: TextStyle(color: Colors.black),
            validator: (value) {
                return (value != null && value.isNotEmpty) ? null : 'Password is required.';
            }
        );

        Widget btnSubmit = Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 10.0),
            child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Colors.red.shade900
                ),
                onPressed: () { 
                    if (_formKey.currentState!.validate()) {
                        _login(context);

                    } else {
                        showSnackBar(context, 'Form validation failed. Check input and try again.');
                    }
                }, 
                child: Text('Login')
            )
        );

        Widget lblAppTitle = Container(
            width: double.infinity,
            margin: EdgeInsets.only(top: 60.0, bottom: 60.0),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                    // Add styles to the texts below (size 30, bold font weight, red color on Login text).
                    Text(
                        'Project Management',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 30

                        ),
                    ),
                    Text(
                        'Login',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 30,
                            color: Colors.red.shade900
                        ),
                    )
                ]
            )
        );

        Widget imgLogo = Expanded(
            child: Padding(
                padding: EdgeInsets.only(bottom: 16.0),
                child: Align(
                    // Align the image according to shown sample.
                    alignment: Alignment.bottomCenter,
                    child: Image.asset('assets/ffuf-logo.png', width: 100)
                )
            )
        );

        Widget formLogin = Form(
            key: _formKey,
            child: Column(
                children: [
                    lblAppTitle,
                    txtEmail,
                    txtPassword,
                    btnSubmit,
                    imgLogo
                ]
            )
        );

        Widget loginView = FutureBuilder(
            future: _futureLogin,
            builder: (context, snapshot) {
                if (_futureLogin == null) {
                    return formLogin;
                } else if (snapshot.hasError == true) {
                    return formLogin;
                } else if (snapshot.hasData == true) {
                    Timer(Duration(milliseconds: 1), () {
                        _goToHomepage(snapshot.data);

                    });

                    return Container();
                }

                return Center(
                    child: CircularProgressIndicator()
                );
            }
        );

        return Scaffold(
            body: Container(
                width: double.infinity,
                padding: EdgeInsets.only(top: 30.0, left: 36.0, right: 36.0),
                child: loginView
            )
        );
    }
}