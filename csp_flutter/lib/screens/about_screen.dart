import 'package:flutter/material.dart';

//My personal imports.
import '/utils/about_cards/contact_card.dart';
import '/utils/about_cards/heading_card.dart';
import '/utils/about_cards/middle_card.dart';

class AboutScreen extends StatelessWidget {
    @override
    Widget build(BuildContext context) {
    
        Widget fabBack = FloatingActionButton(
            onPressed: (){
                Navigator.pushNamed(context, '/project-list');
            },
            child: const Icon(Icons.arrow_back),
            backgroundColor: Colors.red.shade700,
        );
        
        return Scaffold(
            floatingActionButtonLocation:FloatingActionButtonLocation.startDocked,
            appBar: AppBar(
                automaticallyImplyLeading: false,
                title: Text('About the Developer'),
                centerTitle: true,
            ),
            body:SingleChildScrollView(
                child: Column(
                    children: <Widget>[
                        headingCard,
                        middleCard,
                        contactCard
                    ],
                )
            ), 
            floatingActionButton: fabBack, 
        );
    }
}
